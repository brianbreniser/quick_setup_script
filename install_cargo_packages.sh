#!/bin/bash
set -e # fail on anything erroring

echo "***************************************************"
echo "***INFORM*** installing some awesome rust things***"
echo "***************************************************"

cargo_install="cargo install "

cargo_install_list=(
                   ripgrep # seriously fast alternative to grep
                   "fd-find" # fast, and more convenient, alternative to find
                   exa # a better ls
                   lsd # another bettert ls, but requires fancy fonts installed, so can't use this by default
                   loc # fast "line of code" counter
                   xsv # cvs formatter, sorter, searcher, etc.
                   bat # better cat
                   toastify # easily send quick and painless desktop notifications
                   eva # evaluate math, a good simple lightweight command line calculator
                   dust # Better du command
                  )

echo "*****************************************"
echo "***INFORM*** Installing Cargo packages***"
echo "*****************************************"
for i in ${cargo_install_list[@]}
    do
        eval "$cargo_install $i" # AHHHHH INSTALL SECTION BE SO CAREFUL HERE I'M SERIOUS -----------------------------------------
    done

echo "**********************************************"
echo "***INFORM*** done installing cargo packages***"
echo "**********************************************"


